#!/bin/sh
set -e
alias cr="cargo run $* --bin"
SEED=0
#SEED=`shuf -i 0-100000 -n 1`
#SEED=`cat seed.txt`
echo $SEED > seed.txt
cargo build
cr librae-cli -- universegen --seed $SEED --fixed-population 1000000 --colonization-packages sunrays > puniversem.mpack &&
cr librae-cli -- moduniverse make-turn 5 < puniversem.mpack > puniverse.mpack &&
export EID=`cr librae-cli -- find -u empire any < puniverse.mpack` &&
cr librae-cli -- viewuniverse -e $EID --perfect < puniverse.mpack > pview.mpack &&
cd librae-gui &&
cr librae-gui -- load-view < ../pview.mpack
