use anyhow::Result;
use clap::Parser;
use librae_cli::*;

#[derive(clap::Parser)]
#[clap(about = "RUSTORION")]
enum Options {
	Act(act::Options),
	Actiongen(actiongen::Options),
	Auth(auth::Options),
	Client(client::Options),
	Find(find::Options),
	Moduniverse(moduniverse::Options),
	Namegen(namegen::Options),
	Server(server::Options),
	Universegen(universegen::Options),
	Viewuniverse(viewuniverse::Options),
}

fn main() -> Result<()> {
	rustorion::ensure_tracing_subscriber();
	let options = Options::parse();

	match options {
		Options::Act(options) => act::main(options),
		Options::Actiongen(options) => actiongen::main(options),
		Options::Auth(options) => auth::main(options),
		Options::Client(options) => client::main(options),
		Options::Find(options) => find::main(options),
		Options::Moduniverse(options) => moduniverse::main(options),
		Options::Namegen(options) => namegen::main(options),
		Options::Server(options) => server::main(options),
		Options::Universegen(options) => universegen::main(options),
		Options::Viewuniverse(options) => viewuniverse::main(options),
	}
}
