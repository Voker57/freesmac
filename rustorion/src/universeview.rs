pub mod interface;

// A non-perfect view into an Universe. Contains only things visible to a certain observer.
// Structure is based on Universe
// Intended to be passed to players, only containing data they are supposed to know

use crate::storage::*;
use crate::units::*;
use crate::universe;
use anyhow::Result;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(PartialEq, Eq, Clone, Copy, Serialize, Deserialize, Hash, Default)]
pub enum WinState {
	Winner(ID<Empire>),
	Draw,
	#[default]
	NoWinner,
}

type UniverseLocation = universe::UniverseLocation;

#[derive(PartialEq, Clone, Eq, Serialize, Deserialize, Debug)]
pub struct Planet {
	pub id: ID<Planet>,
	pub core_spin_speed: Unit<CoreSpinSpeed>,
	pub surface_bio_quality: Unit<SurfaceBioQuality>,
	pub atmosphere_bio_quality: Unit<AtmosphereBioQuality>,
	pub fuel_availability: Rational,
	pub population: Unit<Population>,
	pub number: i64,
	pub goods_storage: GoodsStorage,
	pub planetary_economics: PlanetaryEconomics,
	pub economic_asset_list: EconomicAssetList,
}

impl IDentified for Planet {
	fn id(&self) -> ID<Self> {
		self.id
	}

	fn id_mut(&mut self) -> &mut ID<Self> {
		&mut self.id
	}
}

impl EntityStored<Planet> for Universe {
	fn storage(&self) -> &HashMap<StorageID, Planet> {
		&self.planets
	}
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, Planet> {
		&mut self.planets
	}
}

#[derive(PartialEq, Eq, Clone, Copy, Serialize, Debug, Deserialize)]
pub enum StarType {
	/// White Dwarf -- "ocean", logistics friendly
	White,
	/// G-Type/yellow dwarf -- agricultural/habitation
	Yellow,
	/// Red Giant -- industry
	Red,
	/// Neutron stars -- exotic conditions, science
	Blue,
}

impl std::fmt::Display for StarType {
	fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error> {
		let s = match self {
			StarType::White => "White Dwarf",
			StarType::Yellow => "G-Type",
			StarType::Red => "Red Giant",
			StarType::Blue => "Neutron",
		};
		write!(fmt, "{}", s)
	}
}

impl From<universe::StarType> for StarType {
	fn from(st: universe::StarType) -> Self {
		match st {
			universe::StarType::White => StarType::White,
			universe::StarType::Yellow => StarType::Yellow,
			universe::StarType::Red => StarType::Red,
			universe::StarType::Blue => StarType::Blue,
		}
	}
}

#[derive(PartialEq, Eq, Clone, Serialize, Deserialize)]
pub struct StarSystem {
	pub id: ID<StarSystem>,
	pub name: String,
	pub location: UniverseLocation,
	pub star_cosmic_ray_intensity: Unit<CosmicRayIntensity>,
	pub star_type: StarType,
}

impl IDentified for StarSystem {
	fn id(&self) -> ID<Self> {
		self.id
	}

	fn id_mut(&mut self) -> &mut ID<Self> {
		&mut self.id
	}
}

impl EntityStored<StarSystem> for Universe {
	fn storage(&self) -> &HashMap<StorageID, StarSystem> {
		&self.star_systems
	}
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, StarSystem> {
		&mut self.star_systems
	}
}

#[derive(PartialEq, Eq, Clone, Serialize, Deserialize)]
pub struct Empire {
	pub id: ID<Empire>,
	pub name: String,
	pub color: crate::color::Color,
	// percentage, to avoid precision issues
	pub tax_rate: u64,
	pub empire_economics: EmpireEconomics,
	pub goods_storage: GoodsStorage,
}

impl IDentified for Empire {
	fn id(&self) -> ID<Self> {
		self.id
	}

	fn id_mut(&mut self) -> &mut ID<Self> {
		&mut self.id
	}
}

impl EntityStored<Empire> for Universe {
	fn storage(&self) -> &HashMap<StorageID, Empire> {
		&self.empires
	}
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, Empire> {
		&mut self.empires
	}
}

#[derive(PartialEq, Eq, Clone, Debug, Serialize, Deserialize, Default)]
pub struct EmpireEconomics {
	pub tax_income: Unit<EnergyCredit>,
}

#[derive(PartialEq, Eq, Debug, Serialize, Clone, Deserialize)]
pub struct Fleet {
	pub id: ID<Fleet>,
	pub name: String,
}

impl IDentified for Fleet {
	fn id(&self) -> ID<Self> {
		self.id
	}

	fn id_mut(&mut self) -> &mut ID<Self> {
		&mut self.id
	}
}

impl EntityStored<Fleet> for Universe {
	fn storage(&self) -> &HashMap<StorageID, Fleet> {
		&self.fleets
	}
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, Fleet> {
		&mut self.fleets
	}
}

type ShipModel = universe::ShipModel;

#[derive(PartialEq, Eq, Clone, Serialize, Deserialize)]
pub struct Ship {
	pub id: ID<Ship>,
	pub name: String,
	pub model: ShipModel,
}

impl IDentified for Ship {
	fn id(&self) -> ID<Self> {
		self.id
	}
	fn id_mut(&mut self) -> &mut ID<Self> {
		&mut self.id
	}
}

impl EntityStored<Ship> for Universe {
	fn storage(&self) -> &HashMap<StorageID, Ship> {
		&self.ships
	}
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, Ship> {
		&mut self.ships
	}
}

#[derive(Serialize, Clone, Deserialize, PartialEq, Default)]
pub struct Universe {
	pub fleets: HashMap<StorageID, Fleet>,
	pub planets: HashMap<StorageID, Planet>,
	pub star_systems: HashMap<StorageID, StarSystem>,
	pub empires: HashMap<StorageID, Empire>,
	pub planets_in_star_systems: links::HasMany<StarSystem, Planet>,
	pub starlanes: links::Interlink<StarSystem>,
	pub star_systems_in_empires: links::HasMany<Empire, StarSystem>,
	pub ships: HashMap<StorageID, Ship>,
	pub fleets_in_star_systems: links::HasMany<StarSystem, Fleet>,
	pub fleets_in_empires: links::HasMany<Empire, Fleet>,
	pub capitals_in_empires: links::HasOne<Empire, StarSystem>,
	pub controlled_empire: Option<ID<Empire>>,
	pub turn_number: u64,
	pub win_state: WinState,
	pub width: u64,
	pub height: u64,
	pub ship_cost: Unit<EnergyCredit>,
	pub migration_events: Vec<MigrationEvent>,
	pub ships_in_fleets: links::HasMany<Fleet, Ship>,
	/// Basic cost to transfer 10 cm^3 through a breach between two systems using commercial bulk transport, EC/cm3
	pub breach_shipping_cost: Rational,
	pub void_church: VoidChurch,
	pub trading_parents_maps: TradingParentsMaps,
	pub trade_transaction_data: TradeTransactionData,
}

impl std::fmt::Debug for Universe {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.debug_struct("UniverseView").finish()
	}
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Default)]
pub struct TradeTransactionData {
	pub storage: HashMap<StorageID, TradeTransaction>,
	pub index_turn: crate::storage::index::UnorderedUniqueIndex<u64, TradeTransaction>,
	pub index_economy_agent: crate::storage::index::UnorderedUniqueIndex<EconomyAgent, TradeTransaction>,
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Default)]
/// currently an unique entity working as a bank
pub struct VoidChurch {
	pub goods_storage: GoodsStorage,
}

type TradingParentsMap = HashMap<ID<StarSystem>, HashMap<ID<StarSystem>, (ID<StarSystem>, Rational)>>;
type TradingParentsMaps = HashMap<Option<ID<Empire>>, TradingParentsMap>;

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Copy)]
pub struct TradeTransaction {
	pub source_economy_agent: EconomyAgent,
	pub target_economy_agent: EconomyAgent,
	pub cargo: Cargo,
	pub shipping_fee: Unit<EnergyCredit>,
	pub income: Unit<EnergyCredit>,
	pub turn_number: u64,
	pub id: ID<TradeTransaction>,
}

impl IDentified for TradeTransaction {
	fn id(&self) -> ID<Self> {
		self.id
	}

	fn id_mut(&mut self) -> &mut ID<Self> {
		&mut self.id
	}
}

impl EntityStored<TradeTransaction> for Universe {
	fn storage(&self) -> &HashMap<StorageID, TradeTransaction> {
		&self.trade_transaction_data.storage
	}
	fn storage_mut(&mut self) -> &mut HashMap<StorageID, TradeTransaction> {
		&mut self.trade_transaction_data.storage
	}
	fn index(&mut self, id: ID<TradeTransaction>) -> Result<()> {
		let trade_transaction = *self.getf(id)?;
		self.trade_transaction_data.index_turn.add_index(trade_transaction.turn_number, trade_transaction.id())?;
		self.trade_transaction_data
			.index_economy_agent
			.add_index(trade_transaction.source_economy_agent, trade_transaction.id())?;
		self.trade_transaction_data
			.index_economy_agent
			.add_index(trade_transaction.target_economy_agent, trade_transaction.id())
	}
	fn unindex(&mut self, id: ID<TradeTransaction>) -> Result<()> {
		let trade_transaction = *self.getf(id)?;
		self.trade_transaction_data.index_turn.remove_index(trade_transaction.turn_number, trade_transaction.id())?;
		self.trade_transaction_data
			.index_economy_agent
			.remove_index(trade_transaction.source_economy_agent, trade_transaction.id())?;
		self.trade_transaction_data
			.index_economy_agent
			.remove_index(trade_transaction.target_economy_agent, trade_transaction.id())
	}
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Copy)]
pub struct MigrationEvent {
	pub source_planet_id: ID<Planet>,
	pub target_planet_id: ID<Planet>,
	pub fee: Unit<EnergyCredit>,
	pub population: Unit<Population>,
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Copy)]
pub enum Cargo {
	Food(Unit<Food>),
	Fuel(Unit<Fuel>),
}

impl std::fmt::Display for Cargo {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			Cargo::Food(units) => write!(f, "a shipment of {}", units),
			Cargo::Fuel(units) => write!(f, "a shipment of {}", units),
		}
	}
}

impl From<universe::Cargo> for Cargo {
	fn from(value: universe::Cargo) -> Self {
		match value {
			universe::Cargo::Food(u) => Cargo::Food(u),
			universe::Cargo::Fuel(u) => Cargo::Fuel(u),
		}
	}
}

impl Universe {
	// Shorthand to create an interface instance
	pub fn interface(&self) -> interface::Universe {
		interface::Universe::new(self)
	}

	// Information that only a certain Empire would know
	pub fn empire_view(universe: &universe::Universe, empire_id: ID<universe::Empire>) -> Universe {
		let mut u_v = Universe {
			width: universe.width,
			height: universe.height,
			ship_cost: universe.ship_cost,
			..Default::default()
		};

		for e in universe.empires.values() {
			u_v.import_empire(e);
		}

		for star_system in universe.star_systems.values() {
			// TODO: make more far-reaching import, not only for data
			let imported_star_system_id = u_v.import_star_system(star_system);
			if let Some(owner_empire_id) = universe.star_systems_in_empires.parent(star_system.id) {
				u_v.star_systems_in_empires.add_link(owner_empire_id.cast(), imported_star_system_id);
			}
			if let Some(capital_owner_id) = universe.capitals_in_empires.parent(star_system.id) {
				u_v.capitals_in_empires.add_link(capital_owner_id.cast(), star_system.id.cast());
			}
			for planet_id in &universe.planets_in_star_systems.children(star_system.id) {
				let planet = universe.getf(*planet_id).unwrap();
				let imported_planet_id = u_v.import_planet(planet);
				u_v.planets_in_star_systems.add_link(imported_star_system_id, imported_planet_id);
			}

			for fleet_id in &universe.fleets_in_star_systems.children(star_system.id) {
				let fleet = universe.getf(*fleet_id).unwrap();
				let imported_fleet_id = u_v.import_fleet(fleet);
				for ship_id in &universe.ships_in_fleets.children(fleet.id) {
					u_v.import_ship(universe.getf(*ship_id).unwrap());
					u_v.ships_in_fleets.add_link(imported_fleet_id, ship_id.cast());
				}
				if let Some(fleet_empire_id) = universe.fleets_in_empires.parent(fleet.id) {
					u_v.fleets_in_empires.add_link(fleet_empire_id.cast(), imported_fleet_id);
				}
				u_v.fleets_in_star_systems.add_link(imported_star_system_id, imported_fleet_id);
			}
		}

		for (id, child_map) in &universe.starlanes.map {
			for child_id in child_map {
				u_v.starlanes.add_link(id.cast(), child_id.cast());
			}
		}

		u_v.controlled_empire = Some(empire_id.cast());
		u_v.turn_number = universe.turn_number;
		u_v.import_win_state(universe);
		u_v.import_trade_transactions(universe);
		u_v.import_migration_events(universe);
		u_v.import_trading_parents(universe);
		u_v
	}

	fn import_trading_parents(&mut self, universe: &crate::universe::Universe) {
		self.trading_parents_maps = universe
			.trading_parents_maps
			.clone()
			.into_iter()
			.map(|(empire_option, parents_maps)| {
				(
					empire_option.map(ID::cast),
					parents_maps
						.into_iter()
						.map(|(star_system1_id, parents_map)| {
							(
								star_system1_id.cast(),
								parents_map
									.into_iter()
									.map(|(star_system2_id, (parent_id, cost))| (star_system2_id.cast(), (parent_id.cast(), cost)))
									.collect(),
							)
						})
						.collect(),
				)
			})
			.collect();
	}

	fn import_win_state(&mut self, universe: &crate::universe::Universe) {
		self.win_state = match universe.win_state {
			crate::universe::WinState::Winner(empire_id) => WinState::Winner(empire_id.cast()),
			crate::universe::WinState::NoWinner => WinState::NoWinner,
			crate::universe::WinState::Draw => WinState::Draw,
		}
	}

	// not efficient, use copy_view() (TBD) instead
	// reference view implementation, intended as base for specific views
	pub fn perfect_view(universe: &universe::Universe) -> Universe {
		let mut u = Universe {
			width: universe.width,
			height: universe.height,
			ship_cost: universe.ship_cost,
			breach_shipping_cost: universe.breach_shipping_cost,
			..Default::default()
		};

		for star_system in universe.star_systems.values() {
			u.import_star_system(star_system);
		}

		for planet in universe.planets.values() {
			u.import_planet(planet);
		}

		for empire in universe.empires.values() {
			u.import_empire(empire);
		}

		for fleet in universe.fleets.values() {
			u.import_fleet(fleet);
		}

		for ship in universe.ships.values() {
			u.import_ship(ship);
		}

		for (id, child_map) in &universe.starlanes.map {
			for child_id in child_map {
				u.starlanes.add_link(id.cast(), child_id.cast());
			}
		}

		for (id, child_map) in &universe.planets_in_star_systems.children_mmap {
			for child_id in child_map {
				u.planets_in_star_systems.add_link(id.cast(), child_id.cast());
			}
		}

		for (id, child_id) in &universe.capitals_in_empires.bimap {
			u.capitals_in_empires.add_link(id.cast(), child_id.cast());
		}

		for (id, child_map) in &universe.star_systems_in_empires.children_mmap {
			for child_id in child_map {
				u.star_systems_in_empires.add_link(id.cast(), child_id.cast());
			}
		}

		for (id, child_map) in &universe.fleets_in_empires.children_mmap {
			for child_id in child_map {
				u.fleets_in_empires.add_link(id.cast(), child_id.cast());
			}
		}

		for (id, child_map) in &universe.fleets_in_star_systems.children_mmap {
			for child_id in child_map {
				u.fleets_in_star_systems.add_link(id.cast(), child_id.cast());
			}
		}

		for (id, child_map) in &universe.ships_in_fleets.children_mmap {
			for child_id in child_map {
				u.ships_in_fleets.add_link(id.cast(), child_id.cast());
			}
		}

		u.turn_number = universe.turn_number;
		u.import_win_state(universe);
		u.import_trade_transactions(universe);
		u.import_migration_events(universe);
		u.import_trading_parents(universe);
		u
	}

	pub fn import_star_system(&mut self, star_system: &universe::StarSystem) -> ID<StarSystem> {
		let id = star_system.id().cast();
		let ss = StarSystem {
			id,
			name: star_system.name.clone(),
			location: star_system.location,
			star_cosmic_ray_intensity: star_system.star_cosmic_ray_intensity,
			star_type: star_system.star_type.into(),
		};

		self.set(ss).unwrap();
		id
	}

	pub fn import_economic_asset_list(economic_asset_list: &universe::EconomicAssetList) -> EconomicAssetList {
		economic_asset_list.into()
	}

	pub fn import_planet(&mut self, planet: &universe::Planet) -> ID<Planet> {
		let id = planet.id().cast();
		let p = Planet {
			id,
			core_spin_speed: planet.core_spin_speed,
			surface_bio_quality: planet.surface_bio_quality,
			atmosphere_bio_quality: planet.atmosphere_bio_quality,
			population: planet.population,
			number: planet.number,
			goods_storage: planet.goods_storage.clone().into(),
			planetary_economics: PlanetaryEconomics {
				nils: planet.planetary_economics.nils,
				farmers: planet.planetary_economics.farmers,
				ec_gatherers: planet.planetary_economics.ec_gatherers,
			},
			fuel_availability: planet.fuel_availability,
			economic_asset_list: Self::import_economic_asset_list(&planet.economic_asset_list),
		};

		self.set(p).unwrap();
		id
	}

	pub fn import_ship(&mut self, ship: &universe::Ship) -> ID<Ship> {
		let id = ship.id().cast();
		let s = Ship {
			id,
			name: ship.name.clone(),
			model: ship.model,
		};

		self.set(s).unwrap();
		id
	}

	pub fn import_fleet(&mut self, fleet: &universe::Fleet) -> ID<Fleet> {
		let id = fleet.id().cast();
		let s = Fleet { id, name: fleet.name.clone() };

		self.set(s).unwrap();
		id
	}

	pub fn import_empire(&mut self, empire: &universe::Empire) -> ID<Empire> {
		let id = empire.id().cast();
		let e = Empire {
			id,
			name: empire.name.clone(),
			color: empire.color,
			tax_rate: empire.tax_rate,
			empire_economics: EmpireEconomics {
				tax_income: empire.empire_economics.tax_income,
			},
			goods_storage: empire.goods_storage.clone().into(),
		};

		self.set(e).unwrap();
		id
	}

	pub fn import_trade_transaction(&mut self, trade_transaction: universe::TradeTransaction) -> ID<TradeTransaction> {
		let id = trade_transaction.id.cast();
		self.set(TradeTransaction {
			source_economy_agent: trade_transaction.source_economy_agent.into(),
			target_economy_agent: trade_transaction.target_economy_agent.into(),
			cargo: trade_transaction.cargo.into(),
			shipping_fee: trade_transaction.shipping_fee,
			income: trade_transaction.income,
			id,
			turn_number: trade_transaction.turn_number,
		})
		.unwrap();
		id
	}

	pub fn import_trade_transactions(&mut self, u: &universe::Universe) {
		for trade_transaction in EntityStored::<universe::TradeTransaction>::storage(u).values() {
			self.import_trade_transaction(*trade_transaction);
		}
	}

	pub fn import_migration_events(&mut self, u: &universe::Universe) {
		let mut imported_migration_events = Vec::with_capacity(u.migration_events.len());
		for migration_event in &u.migration_events {
			imported_migration_events.push(MigrationEvent {
				source_planet_id: migration_event.source_planet_id.cast(),
				target_planet_id: migration_event.target_planet_id.cast(),
				fee: migration_event.fee,
				population: migration_event.population,
			});
		}
		self.migration_events = imported_migration_events;
	}
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, Default, Copy)]
pub struct PlanetaryEconomics {
	pub nils: Unit<Population>,
	pub farmers: Unit<Population>,
	pub ec_gatherers: Unit<Population>,
}

#[derive(Clone, Hash, PartialEq, Eq, Copy, Debug, Serialize, Deserialize)]
pub enum EconomyAgent {
	Planet(ID<Planet>),
	Fleet(ID<Fleet>),
	Government(ID<Empire>),
	VoidChurchBranch(ID<Empire>, ID<StarSystem>),
}

impl From<universe::EconomyAgent> for EconomyAgent {
	fn from(economy_agent: universe::EconomyAgent) -> Self {
		match economy_agent {
			universe::EconomyAgent::Planet(id) => EconomyAgent::Planet(id.cast()),
			universe::EconomyAgent::Fleet(id) => EconomyAgent::Fleet(id.cast()),
			universe::EconomyAgent::Government(id) => EconomyAgent::Government(id.cast()),
			universe::EconomyAgent::VoidChurchBranch(empire_id, star_system_id) => EconomyAgent::VoidChurchBranch(empire_id.cast(), star_system_id.cast()),
		}
	}
}

#[derive(Clone, PartialEq, Eq, Default, Serialize, Deserialize, Debug)]
pub struct GoodsStorage {
	pub food: Unit<Food>,
	pub energy_credits: Unit<EnergyCredit>,
	pub fuel: Unit<Fuel>,
}

impl From<universe::GoodsStorage> for GoodsStorage {
	fn from(goods_storage: universe::GoodsStorage) -> Self {
		GoodsStorage {
			food: goods_storage.food,
			energy_credits: goods_storage.energy_credits,
			fuel: goods_storage.fuel,
		}
	}
}

#[derive(Clone, Serialize, Deserialize, Default, Eq, PartialEq, Ord, PartialOrd, Debug)]
pub struct EconomicAssetList {
	pub farms: Unit<Farm>,
	pub fuel_sifters: Unit<FuelSifter>,
}

impl From<&universe::EconomicAssetList> for EconomicAssetList {
	fn from(economic_asset_list: &universe::EconomicAssetList) -> Self {
		Self {
			farms: economic_asset_list.farms,
			fuel_sifters: economic_asset_list.fuel_sifters,
		}
	}
}
