use crate::storage::*;
use anyhow::{bail, Result};

#[derive(Debug, Clone, Eq, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct UnorderedUniqueIndex<Key: Hash + Eq, Entity> {
	pub map: std::collections::HashMap<Key, std::collections::HashSet<ID<Entity>>, std::collections::hash_map::RandomState>,
}

impl<Key: Hash + Eq, Entity> Default for UnorderedUniqueIndex<Key, Entity> {
	fn default() -> Self {
		Self { map: Default::default() }
	}
}

impl<Key: Hash + Eq, Entity> UnorderedUniqueIndex<Key, Entity> {
	pub fn add_index(&mut self, key: Key, entity_id: ID<Entity>) -> Result<()> {
		if self.map.entry(key).or_default().insert(entity_id) {
			Ok(())
		} else {
			bail!("index already present")
		}
	}

	pub fn remove_index(&mut self, key: Key, entity_id: ID<Entity>) -> Result<()> {
		if self.map.get_mut(&key).expect("attempting to unindex unindexed entry").remove(&entity_id) {
			Ok(())
		} else {
			bail!("index not present")
		}
	}
}
