// Name generation
// language based on finnish phonetics, with enforced order of vowels and consonants
// only easy-to-pronounce (subjective) doubles are permitted
use rand::seq::SliceRandom;
extern crate rand;
use anyhow::{anyhow, Result};

const VOWELS: [&str; 5] = ["a", "e", "u", "i", "o"];
const DIVOWELS: [&str; 25] = [
	"aa", "ae", "au", "ai", "ao", "ea", "ee", "eu", "ei", "eo", "ua", "ue", "uu", "ui", "uo", "ia", "ie", "iu", "ii", "io", "oa", "oe", "ou", "oi", "oo",
];
const CONSONANTS: [&str; 15] = ["r", "t", "p", "s", "d", "f", "g", "h", "k", "l", "z", "v", "b", "n", "m"];
const DICONSONANTS: [&str; 143] = [
	"rr", "rt", "rp", "rs", "rd", "rf", "rg", "rh", "rk", "rl", "rz", "rv", "rb", "rn", "rm", "tt", "ts", "tk", "tl", "tz", "pt", "ps", "pk", "pl", "sr", "st", "sp", "ss", "sf", "sk", "sl", "sn",
	"sm", "dr", "ds", "dd", "df", "dg", "dh", "dl", "dz", "dv", "fr", "ft", "fs", "ff", "fk", "fl", "fn", "fm", "gr", "gs", "gf", "gl", "gz", "gn", "ht", "hp", "hs", "hf", "hg", "hh", "hk", "hl",
	"hz", "hv", "hn", "hm", "kr", "kt", "ks", "kf", "kh", "kk", "kl", "kz", "kv", "kn", "km", "ls", "ld", "lf", "lg", "lh", "lk", "ll", "lz", "lv", "lb", "ln", "lm", "zr", "zl", "zz", "zb", "zn",
	"zm", "vr", "vd", "vg", "vl", "vz", "vv", "vn", "vm", "br", "bg", "bh", "bl", "bz", "bb", "bn", "bm", "nr", "nt", "np", "ns", "nd", "nf", "ng", "nh", "nk", "nl", "nz", "nv", "nb", "nn", "nm",
	"mr", "mt", "mp", "ms", "md", "mf", "mg", "mh", "mk", "ml", "mz", "mv", "mb", "mn", "mm",
];

const NAMES: [&str; 188] = [
	"Anguilla",
	"Antinous",
	"Apes",
	"Apis",
	"Aranea",
	"Argo Navis",
	"Asselli and Praesepe",
	"Asterion and Chara",
	"Battery of Volta",
	"Bufo",
	"Cancer Minor",
	"Capra and Haedi",
	"Cerberus",
	"Cor Caroli Regis Martyris",
	"Corona Firmiana",
	"Custos Messium",
	"Deltoton",
	"Dentalium",
	"Felis",
	"Frederici Honores",
	"Gallus",
	"Gladii Electorales Saxonici",
	"Globus Aerostaticus",
	"Gryphites",
	"Hippocampus",
	"Hirudo",
	"Jordanus",
	"Leo Palatinus",
	"Lilium",
	"Limax",
	"Linum Piscium",
	"Lochium Funis",
	"Lumbricus",
	"Machina Electrica",
	"Malus",
	"Manis",
	"Marmor Sculptile",
	"Mons Maenalus",
	"Musca Borealis",
	"Noctua",
	"Nubecula Major",
	"Nubecula Minor",
	"Officina Typographica",
	"Patella",
	"Phaethon",
	"Phoenicopterus",
	"Pinna Marina",
	"Piscis Notus",
	"Pluteum",
	"Polophylax",
	"Pomum Imperiale",
	"Psalterium Georgii",
	"Quadrans Muralis",
	"Quadratum",
	"Ramus Pomifer",
	"Robur Carolinum",
	"Rosa",
	"Sagitta Australis",
	"Scarabaeus",
	"Sceptrum Brandenburgicum",
	"Sceptrum et Manus Iustitiae",
	"Sciurus Volans",
	"Sextans Uraniae",
	"Siren",
	"Ceneus",
	"Lang",
	"Solarium",
	"Sudarium Veronicae",
	"Tarabellum",
	"Vexillum",
	"Tarandus or Rangifer",
	"Taurus Poniatovii",
	"Voker Pedik",
	"Telescopium Herschelii",
	"Testudo",
	"Tigris",
	"Triangulum Majus",
	"Triangulum Minus",
	"Triangulus Antarcticus",
	"Tubus Herschelii Minor",
	"Turdus Solitarius",
	"Uranoscopus",
	"Urna",
	"Vespa",
	"Triangula",
	"Triangulum",
	"Catuli",
	"Corona",
	"Corolla",
	"Piscis",
	"Camelus",
	"Vulpes",
	"Equus",
	"Delphin",
	"Ursa Minor",
	"Canis",
	"Felis",
	"Leaena",
	"Cervus",
	"Andromeda",
	"Antlia",
	"Apus",
	"Aquarius",
	"Aquila",
	"Ara",
	"Argo Navis",
	"Aries",
	"Auriga",
	"Boötes",
	"Caelum",
	"Camelopardalis",
	"Cancer",
	"Canes Venatici",
	"Canis Major",
	"Canis Minor",
	"Capricornus",
	"Carina",
	"Cassiopeia",
	"Centaurus",
	"Cepheus",
	"Cetus",
	"Chamaeleon",
	"Circinus",
	"Columba",
	"Coma Berenices",
	"Corona Australis",
	"Corona Borealis",
	"Corvus",
	"Crater",
	"Crux",
	"Cygnus",
	"Delphinus",
	"Dorado",
	"Draco",
	"Equuleus",
	"Eridanus",
	"Fornax",
	"Gemini",
	"Grus",
	"Hercules",
	"Horologium",
	"Hydra",
	"Hydrus",
	"Indus",
	"Lacerta",
	"Leo",
	"Leo Minor",
	"Lepus",
	"Libra",
	"Lupus",
	"Lynx",
	"Lyra",
	"Mensa",
	"Microscopium",
	"Monoceros",
	"Musca",
	"Norma",
	"Octans",
	"Ophiuchus",
	"Orion",
	"Pavo",
	"Pegasus",
	"Perseus",
	"Phoenix",
	"Pictor",
	"Pisces",
	"Piscis Austrinus",
	"Puppis",
	"Pyxis",
	"Reticulum",
	"Sagitta",
	"Sagittarius",
	"Scorpius",
	"Sculptor",
	"Scutum",
	"Serpens",
	"Sextans",
	"Taurus",
	"Telescopium",
	"Triangulum",
	"Triangulum Australe",
	"Tucana",
	"Ursa Major",
	"Ursa Minor",
	"Vela",
	"Virgo",
	"Volans",
	"Vulpecula",
];

#[derive(Debug, PartialEq, Eq)]
pub enum NameGenerator {
	Autistic,
	Normal,
}
// Generate a name of given length and append a suffix.
// Ensures suffix does not break vowel harmony by appending an extra letter if necessary
pub fn generate_name(rng: &mut impl rand::Rng, length: usize, suffix: &str, gen_type: NameGenerator) -> String {
	let mut word = String::new();
	if gen_type == NameGenerator::Autistic {
		let mut vowel = rng.gen::<bool>();
		fn append_part(rng: &mut impl rand::Rng, word: &mut String, vowel: &mut bool, length: &usize) {
			let double = rng.gen_range(0..100) < 20 && !word.is_empty() && word.len() < length - 1;
			if *vowel {
				let part = if double { DIVOWELS.choose(rng) } else { VOWELS.choose(rng) };
				word.push_str(part.unwrap());
			} else {
				let part = if double { DICONSONANTS.choose(rng) } else { CONSONANTS.choose(rng) };
				word.push_str(part.unwrap());
			}
		}
		while word.len() < length {
			append_part(rng, &mut word, &mut vowel, &length);
			vowel = !vowel;
		}
		if !suffix.is_empty() {
			let suffix_start = suffix.chars().next();
			let vowel_next = VOWELS.iter().any(|c| c.chars().next() == suffix_start);
			let vowel_before = !vowel;
			if vowel_next == vowel_before {
				// Do another round to avoid disallowed neighbors
				append_part(rng, &mut word, &mut vowel, &length);
			};
			word.push_str(suffix);
		}
		word
	} else {
		let chosen_name = NAMES.choose(rng).expect("Array is empty");
		chosen_name.to_string()
	}
}

lazy_static::lazy_static! {
	static ref SANE_REGEX: regex::Regex = regex::Regex::new("^[0-9a-zA-Z\\-_]*$").unwrap();
}
/// check if a string consists of minimum set of expressive yet portable characters
pub fn sanity_check(s: &str) -> Result<()> {
	if SANE_REGEX.is_match(s) {
		Ok(())
	} else {
		Err(anyhow!("a name '{}' is not a sane one", s))
	}
}
