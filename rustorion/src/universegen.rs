// Universe generation
use crate::names;
use crate::storage::EntityStored;
use crate::storage::ID;
use crate::units::*;
use crate::universe;
use anyhow::{anyhow, Result};
use num_traits::ToPrimitive;
use rand::seq::SliceRandom;
use rand::Rng;
use rand::SeedableRng;
use rand_distr::Distribution;
use std::collections::HashSet;
use universe::UniverseLocation;

#[derive(Debug, PartialEq, clap::Parser)]
pub struct UniverseParameters {
	/// number of star systems. Overrides -s
	#[clap(short = 'S', long = "star-systems-total")]
	pub star_systems_total: Option<u64>,
	/// number of star systems per empire
	#[clap(short = 's', long = "star-systems-per-empire", default_value = "50")]
	pub star_systems_per_empire: u64,
	/// number of empires
	#[clap(short = 'e', long = "empires", default_value = "5")]
	pub empires_n: u64,
	/// add some fixed stuff for automatic testing
	#[clap(short = 't')]
	pub add_testables: bool,
	/// rate of uninhabited systems
	#[clap(short = 'u', default_value = "0.2")]
	pub uninhabited: f64,
	/// spawn lots of ships for testing
	#[clap(long = "spawn-lots-of-ships")]
	pub spawn_lots_of_ships: bool,

	/// universe height, in both directions
	#[clap(short = 'H')]
	pub height: Option<u64>,
	/// universe width, in both directions
	#[clap(short = 'W')]
	pub width: Option<u64>,
	#[clap(subcommand)]
	/// generator type
	pub generator: UniverseGenerator,
	/// divide all systems between empires
	#[clap(long)]
	pub molotov_ribbentrop: bool,
	/// assign fixed population to all planets
	#[clap(long)]
	pub fixed_population: Option<u64>,
	/// give colonization packages with fuel and food to all planets
	#[clap(long)]
	pub colonization_packages: bool,
	/// assign automatic population based on insurance price to all planets
	#[clap(long)]
	pub auto_population: bool,
	#[clap(long)]
	pub seed: Option<u64>,
}

impl UniverseParameters {
	fn get_star_systems_per_empire(&self) -> u64 {
		self.star_systems_total.map(|ss| ss.div(self.empires_n)).unwrap_or(self.star_systems_per_empire)
	}

	fn get_star_systems_total(&self) -> u64 {
		self.star_systems_total.unwrap_or(self.star_systems_per_empire * self.empires_n)
	}
}

#[derive(Debug, PartialEq, Eq, clap::Parser)]
pub enum UniverseGenerator {
	/// throws random systems around and connects them
	Haphazard,
	/// generates a sophisticated map with interconnected branches going out from Sol
	Sunrays,
	/// ignores most of the parameters to generate a 3x4 fixed system grid for testing
	TheGrid,
	/// lays out stars in a 5-ended star
	Pentagram,
}

impl Default for UniverseParameters {
	fn default() -> Self {
		UniverseParameters {
			colonization_packages: false,
			star_systems_per_empire: 50,
			star_systems_total: None,
			empires_n: 5,
			uninhabited: 0.2,
			add_testables: false,
			spawn_lots_of_ships: false,
			generator: UniverseGenerator::Haphazard,
			height: None,
			width: None,
			fixed_population: None,
			auto_population: true,
			molotov_ribbentrop: false,
			seed: None,
		}
	}
}

#[derive(Default)]
struct Placer {
	location_set: HashSet<UniverseLocation>,
}

impl Placer {
	fn random_place(&mut self, rng: &mut impl rand::Rng, u: &universe::Universe) -> universe::UniverseLocation {
		let mut location = (0, 0).into();
		let mut first = true;
		while first || self.location_set.contains(&location) {
			let x = rng.gen_range(-100..100) * ((u.width as i64).div(200));
			let y = rng.gen_range(-100..100) * ((u.height as i64).div(200));
			location = (x, y).into();
			first = false;
		}
		self.location_set.insert(location);
		location
	}

	// find a place near the point on edge of concentric squares
	fn closest_place(&mut self, u: &universe::Universe, location: UniverseLocation) -> universe::UniverseLocation {
		if !self.location_set.contains(&location) {
			self.location_set.insert(location);
			return location;
		}
		let mut radius_m = 1;
		let radius = 200;
		while radius * radius_m < u.width.min(u.height) as i64 {
			for x in 0 - radius_m..radius_m {
				for y in 0 - radius_m..radius_m {
					let location = location + (x * radius, y * radius).into();
					if !self.location_set.contains(&location) {
						self.location_set.insert(location);
						return location;
					}
				}
			}
			radius_m += 1;
		}
		panic!("Could not find a place")
	}

	// generate a cluster of star systems centered on the given location
	fn cluster(&mut self, rng: &mut impl rand::Rng, u: &mut universe::Universe, location: UniverseLocation, star_types: &[StarDna]) -> Vec<ID<universe::StarSystem>> {
		star_types
			.iter()
			.map(|star_dna| {
				let ss_location = self.closest_place(u, location);
				generate_star_system(u, ss_location, *star_dna, rng)
			})
			.collect()
	}
}

pub enum Problem {
	TooLittleSystems { recommended: u64 },
	TooLittleEmpires { recommended: u64 },
	TooManyEmpires { recommended: u64 },
}

impl std::fmt::Display for Problem {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			Problem::TooLittleSystems { recommended } => write!(f, "Too little systems for this generator. Recommended minimum is {}", recommended),
			Problem::TooManyEmpires { recommended } => write!(f, "Too many empires for this generator. Recommended maximum is {}", recommended),
			Problem::TooLittleEmpires { recommended } => write!(f, "Too little empires for this generator. Recommended minimum is {}", recommended),
		}
	}
}

#[allow(clippy::single_match)]
pub fn check_params(params: &UniverseParameters) -> Vec<Problem> {
	let mut problems = vec![];
	if params.empires_n > 16 {
		problems.push(Problem::TooManyEmpires { recommended: 16 });
	}
	match params.generator {
		UniverseGenerator::Sunrays => {
			if params.get_star_systems_per_empire() < 10 {
				problems.push(Problem::TooLittleSystems { recommended: 10 })
			}
			if params.empires_n < 2 {
				problems.push(Problem::TooLittleEmpires { recommended: 2 })
			}
		}
		_ => (),
	}
	problems
}

fn sample_triangular_unit<UnitName: UnitType>(min: f64, max: f64, mid: f64, rng: &mut impl rand::Rng) -> Unit<UnitName> {
	rand_distr::Triangular::new(min, max, mid).unwrap().sample(rng).into()
}

pub fn add_planet(u: &mut universe::Universe, star_system_id: ID<universe::StarSystem>) -> &mut universe::Planet {
	let planet = universe::Planet {
		number: u.planets_in_star_systems.children(star_system_id).len() as i64,
		..Default::default()
	};
	let planet_id = u.insert(planet).unwrap();
	u.planets_in_star_systems.add_link(star_system_id, planet_id);
	u.getf_mut(planet_id).unwrap()
}

pub fn generate_good(u: &mut universe::Universe, star_system_id: ID<universe::StarSystem>, rng: &mut impl rand::Rng) -> ID<universe::Planet> {
	let core_spin_speed = sample_triangular_unit(0.9, 1.5, 1.2, rng);
	let surface_bio_quality = sample_triangular_unit(0.8, 1.0, 0.9, rng);
	let atmosphere_bio_quality = sample_triangular_unit(0.8, 1.0, 0.9, rng);

	let planet = add_planet(u, star_system_id);
	planet.core_spin_speed = core_spin_speed;
	planet.surface_bio_quality = surface_bio_quality;
	planet.atmosphere_bio_quality = atmosphere_bio_quality;
	planet.set_natural_fuel_availability();
	planet.id
}

pub fn generate_decent(u: &mut universe::Universe, star_system_id: ID<universe::StarSystem>, rng: &mut impl rand::Rng) -> ID<universe::Planet> {
	let core_spin_speed = sample_triangular_unit(0.5, 1.5, 1.0, rng);
	let surface_bio_quality = sample_triangular_unit(0.5, 1.0, 0.7, rng);
	let atmosphere_bio_quality = sample_triangular_unit(0.5, 0.8, 0.6, rng);

	let planet = add_planet(u, star_system_id);
	planet.core_spin_speed = core_spin_speed;
	planet.surface_bio_quality = surface_bio_quality;
	planet.atmosphere_bio_quality = atmosphere_bio_quality;
	planet.set_natural_fuel_availability();
	planet.id
}

pub fn generate_rock(u: &mut universe::Universe, star_system_id: ID<universe::StarSystem>, rng: &mut impl rand::Rng) -> ID<universe::Planet> {
	let core_spin_speed = sample_triangular_unit(0.0, 0.1, 0.0, rng);
	let surface_bio_quality = sample_triangular_unit(-2., 0.3, 0.0, rng);
	let atmosphere_bio_quality = sample_triangular_unit(-5., 0.0, 0.0, rng);

	let planet = add_planet(u, star_system_id);
	planet.core_spin_speed = core_spin_speed;
	planet.surface_bio_quality = surface_bio_quality;
	planet.atmosphere_bio_quality = atmosphere_bio_quality;
	planet.set_natural_fuel_availability();
	planet.id
}

// Create planet with parameters to ensure its insurance cost is in specified bounds
pub fn clamp_insurance_cost(u: &mut universe::Universe, max: Option<Rational>, planet_id: ID<universe::Planet>, rng: &mut impl rand::Rng, dont_touch_core: bool) {
	let core_spin_speed = u.getf(planet_id).unwrap().core_spin_speed;
	let interface = u.interface();
	let star_system = interface.planet(planet_id).star_system();

	// sub basic insurance
	let budget = max.map(|b| b.sub(Rational::from(10_000)));

	let core_spin_speed = if !dont_touch_core {
		let effective_cosmic_ray_intensity = Unit::<CosmicRayIntensity>::from(1).add(&star_system.data.star_cosmic_ray_intensity);

		let core_spin_speed_min = if let Some(budget) = budget {
			Rational::from(effective_cosmic_ray_intensity).sub(&budget.div(1000)).max(0.into())
		} else {
			Rational::from(0)
		}
		.to_f64()
		.unwrap();

		rand_distr::Triangular::new(core_spin_speed_min, 1.5_f64.max(core_spin_speed_min), 1.0_f64.max(core_spin_speed_min))
			.unwrap()
			.sample(rng)
			.into()
	} else {
		core_spin_speed
	};

	let original_effective_cosmic_ray_intensity = Unit::<CosmicRayIntensity>::from(1).add(&star_system.data.star_cosmic_ray_intensity);
	let effective_cosmic_ray_intensity: Unit<CosmicRayIntensity> = original_effective_cosmic_ray_intensity.sub(&core_spin_speed.cast()).max(0.into());

	let budget = budget.map(|b| b.sub(Rational::from(effective_cosmic_ray_intensity).mul(1000)));

	let surface_bio_quality_min = if let Some(budget) = budget {
		1.0.sub(budget.to_f64().unwrap().div(100_000.0)).clamp(-5., 1.0)
	} else {
		-5.
	};

	let surface_max = 1.0_f64.max(surface_bio_quality_min);
	let surface_bio_quality: Unit<SurfaceBioQuality> = rand_distr::Triangular::new(surface_bio_quality_min, surface_max, 0_f64.clamp(surface_bio_quality_min, surface_max))
		.unwrap()
		.sample(rng)
		.into();

	let budget = budget.map(|b| b.sub(Rational::from(1).sub(Rational::from(surface_bio_quality)).mul(100_000)));

	let min_atmosphere: Unit<AtmosphereBioQuality> = if let Some(budget) = budget {
		1.0.sub(budget.to_f64().unwrap().div(100_000.0)).clamp(-5.0, 1.0)
	} else {
		-5.0
	}
	.into();

	// check if spin speed is too low to accomodate for good enough atmosphere
	let core_spin_speed = if min_atmosphere.cast() > core_spin_speed { min_atmosphere.cast() } else { core_spin_speed };

	let atmosphere_bio_quality = rand_distr::Triangular::new(min_atmosphere.into(), 1.0, 0_f64.max(min_atmosphere.into())).unwrap().sample(rng).into();

	assert!(budget.is_none() || budget.is_some_and(|b| b >= 0.into()));

	let planet = u.getf_mut(planet_id).unwrap();
	planet.core_spin_speed = core_spin_speed;
	planet.surface_bio_quality = surface_bio_quality;
	planet.atmosphere_bio_quality = atmosphere_bio_quality;
	planet.set_natural_fuel_availability();
}

pub fn generate_star_system(u: &mut universe::Universe, star_system_location: UniverseLocation, star_dna: StarDna, rng: &mut impl rand::Rng) -> ID<universe::StarSystem> {
	match star_dna {
		StarDna::Random(star_type) => {
			let star_cosmic_ray_distribution = match star_type {
				universe::StarType::White => rand_distr::Triangular::new(2.0, 4.0, 3.0).unwrap(),
				universe::StarType::Blue => rand_distr::Triangular::new(3.0, 6.0, 5.0).unwrap(),
				universe::StarType::Red => rand_distr::Triangular::new(1.0, 3.0, 1.2).unwrap(),
				universe::StarType::Yellow => rand_distr::Triangular::new(0.7, 1.3, 1.0).unwrap(),
			};
			let star_cosmic_ray_intensity = star_cosmic_ray_distribution.sample(rng).into();
			let star_system = universe::StarSystem {
				id: ID::invalid(),
				name: format!(
					"{} {}",
					names::generate_name(rng, 4, "", names::NameGenerator::Autistic),
					names::generate_name(rng, 4, "", names::NameGenerator::Autistic)
				),
				location: star_system_location,
				star_cosmic_ray_intensity,
				star_type,
			};
			let star_system_id = u.insert(star_system).unwrap();

			match star_type {
				universe::StarType::White => generate_rock(u, star_system_id, rng),
				universe::StarType::Blue => generate_rock(u, star_system_id, rng),
				universe::StarType::Red => {
					if rng.gen::<f32>() > 0.7 {
						generate_decent(u, star_system_id, rng)
					} else {
						generate_rock(u, star_system_id, rng)
					}
				}
				universe::StarType::Yellow => {
					if rng.gen::<f32>() > 0.7 {
						generate_good(u, star_system_id, rng)
					} else {
						generate_rock(u, star_system_id, rng)
					}
				}
			};
			star_system_id
		}
		StarDna::ColonialCapital => {
			let star_cosmic_ray_distribution = rand_distr::Triangular::new(0.9, 1.1, 1.0).unwrap();
			let star_cosmic_ray_intensity = star_cosmic_ray_distribution.sample(rng).into();
			let star_system = universe::StarSystem {
				id: ID::invalid(),
				name: format!(
					"{} {} {}",
					names::generate_name(rng, 6, "", names::NameGenerator::Autistic),
					names::generate_name(rng, 7, "", names::NameGenerator::Autistic),
					names::generate_name(rng, 4, "", names::NameGenerator::Autistic)
				),
				location: star_system_location,
				star_cosmic_ray_intensity,
				star_type: universe::StarType::Yellow,
			};
			let star_system_id = u.insert(star_system).unwrap();
			let garden_id = generate_good(u, star_system_id, rng);
			clamp_insurance_cost(u, Some((100_000).into()), garden_id, rng, false);
			let moon_id = generate_rock(u, star_system_id, rng);
			clamp_insurance_cost(u, Some((200_000).into()), moon_id, rng, true);
			star_system_id
		}
	}
}

/// A vector of nodes containing information about star systems and branches coming out of them, prepared for setting locations
#[derive(Default, Clone)]
struct Span {
	nodes: Vec<Node>,
}

/// A description of a system to generate
#[derive(Clone, Copy)]
pub enum StarDna {
	ColonialCapital,
	Random(universe::StarType),
}

impl Default for StarDna {
	fn default() -> Self {
		StarDna::Random(universe::StarType::White)
	}
}

/// A star system and branches that should come out of it
#[derive(Default, Clone)]
struct Node {
	stars: Vec<StarDna>,
	branches: Vec<Span>,
}

impl Node {}

/// How many things should be added to a node and at which weight
#[derive(Default, Clone)]
struct ExpansionOption {
	size: u64,
	weight: f64,
}

/// Loose description of a span to generate, needs randomized filling
#[derive(Default)]
struct SpanDna {
	branches: Vec<ExpansionOption>,
	clusters: Vec<ExpansionOption>,
	star_dna_weights: Option<Vec<(StarDna, f64)>>,
	spine_weight: f64,
}

impl Span {
	// count of star systems which need to be assigned a type
	fn star_systems_to_be_typed(&self, span_dnas: &[SpanDna], this_is_the_root: bool) -> u64 {
		let current_dna = if let Some(current_dna) = span_dnas.first() {
			current_dna
		} else {
			return 0;
		};
		let rest_dnas = &span_dnas[1..];

		if !this_is_the_root && current_dna.star_dna_weights.is_some() {
			return 0;
		}

		let mut count = 0;
		for node in self.nodes.iter() {
			count = count.add(node.stars.len());
			for span in node.branches.iter() {
				count = count.add(span.star_systems_to_be_typed(rest_dnas, false) as usize)
			}
		}
		count as u64
	}
	fn assign_star_types(&mut self, star_dna_iter: &mut std::iter::Cycle<std::vec::IntoIter<StarDna>>) {
		for node in self.nodes.iter_mut() {
			for star in node.stars.iter_mut() {
				*star = star_dna_iter.next().unwrap();
			}
			for branch in node.branches.iter_mut() {
				branch.assign_star_types(star_dna_iter);
			}
		}
	}

	fn build_from_dna(span_dnas: &[SpanDna], mut budget: f64, rng: &mut impl rand::Rng, branchless_ends: bool) -> (Span, f64) {
		let current_dna = if let Some(current_dna) = span_dnas.first() {
			current_dna
		} else {
			return (Span::default(), budget);
		};
		let rest_dnas = &span_dnas[1..];

		let weights: f64 = current_dna.spine_weight
			+ current_dna
				.branches
				.iter()
				.chain(current_dna.clusters.iter())
				.map(|expansion_option| expansion_option.weight)
				.sum::<f64>();

		let weighted_budget: f64 = budget.div(weights);

		tracing::debug!("Starting span, budget: {budget}");
		let spine_size = weighted_budget.mul(current_dna.spine_weight).ceil().max(1.0);

		let mut span = Span::default();
		span.nodes.resize(
			spine_size as usize,
			Node {
				stars: vec![StarDna::Random(universe::StarType::White)],
				branches: vec![],
			},
		);

		budget = budget.sub(spine_size).max(0.0);

		for expansion_option in &current_dna.clusters {
			let mut expansion_budget = (weighted_budget * expansion_option.weight).round();
			budget = budget.sub(expansion_budget);
			tracing::debug!("expansion budget: {expansion_budget}");
			if expansion_budget == 0.0 {
				continue;
			};
			span.nodes.sort_unstable_by_key(|node| node.stars.len());
			for node in span.nodes.iter_mut() {
				if expansion_budget == 0.0 {
					tracing::debug!("over budget");
					break;
				};

				let node_size = expansion_option.size.min(expansion_budget.ceil() as u64);
				tracing::debug!("+{}e, real: {}", expansion_option.size, node_size);
				expansion_budget = expansion_budget.sub(node_size as f64).max(0.0);
				node.stars.clear();
				node.stars.resize(node_size as usize, StarDna::Random(universe::StarType::White));
			}
			budget = budget.add(expansion_budget);
			tracing::debug!("left over: {expansion_budget}/{budget}");
		}

		for expansion_option in &current_dna.branches {
			let mut expansion_budget = (weighted_budget * expansion_option.weight).round();
			budget = budget.sub(expansion_budget);
			if expansion_budget == 0.0 {
				continue;
			};
			tracing::debug!("branch budget: {expansion_budget}, nodes: {}", span.nodes.len());
			span.nodes.sort_unstable_by_key(|node| node.branches.len());
			for node in span.nodes.iter_mut().skip(if branchless_ends { 2 } else { 0 }) {
				if expansion_budget == 0.0 {
					tracing::debug!("over budget");
					break;
				};
				if node.branches.len() > 1 {
					tracing::debug!("too many branches already");
					break;
				};
				let branch_budget = (expansion_option.size as f64).min(expansion_budget);
				tracing::debug!("requested {} descending for {branch_budget}, dnas left: {}", expansion_option.size, rest_dnas.len());
				expansion_budget = expansion_budget.sub(branch_budget).max(0.0);
				let (span, leftover_budget) = Span::build_from_dna(rest_dnas, branch_budget, rng, true);
				expansion_budget = expansion_budget.add(leftover_budget);
				node.branches.push(span);
			}
			tracing::debug!("left over: {expansion_budget}/{budget}");
			budget = budget.add(expansion_budget);
		}
		if budget != 0.0 {
			tracing::debug!("Span leftover budget: {budget}");
		}
		// put two branchless nodes at the edges
		let penultimate = span.nodes.len() - 1;
		if branchless_ends && span.nodes.len() > 2 {
			span.nodes.sort_unstable_by_key(|node| node.branches.len());
			span.nodes.swap(1, penultimate);
			span.nodes[1..penultimate].shuffle(rng);
		} else {
			span.nodes.shuffle(rng);
		};

		// assign real star types
		if let Some(star_dna_weights) = &current_dna.star_dna_weights {
			let star_systems_to_be_typed = span.star_systems_to_be_typed(span_dnas, true) as f64;
			let mut star_dnas = Vec::with_capacity(star_systems_to_be_typed as usize);
			let weights: f64 = star_dna_weights.iter().map(|w| w.1).sum();
			let star_dnas_weighted_budget = star_systems_to_be_typed.div(weights);
			for (star_dna, weight) in star_dna_weights {
				let count: f64 = weight.mul(star_dnas_weighted_budget);
				star_dnas.extend(std::iter::repeat(star_dna).take(count.round() as usize));
			}
			star_dnas.shuffle(rng);
			let mut star_dna_iter = star_dnas.into_iter().cycle();
			span.assign_star_types(&mut star_dna_iter);
		};

		(span, budget)
	}
}

/// Place star systems of a span of a ray centered in 0x0 and starting in given offset, based on its plan
/// returns the spine nodes and ending offset
#[allow(clippy::type_complexity)]
#[allow(clippy::too_many_arguments)]
fn lay_out_span(
	angle: f32,
	ray_angle_step: f32,
	mut current_offset: f32,
	next_offset: f32,
	span: &Span,
	u: &mut universe::Universe,

	placer: &mut Placer,
	rng: &mut impl Rng,
) -> Vec<(UniverseLocation, Vec<ID<universe::StarSystem>>)> {
	let mut locations_ss: Vec<(UniverseLocation, Vec<ID<universe::StarSystem>>)> = Vec::with_capacity(span.nodes.len());
	let offset_step = next_offset.sub(current_offset).div(span.nodes.len() as f32);
	for node in &span.nodes {
		current_offset = current_offset.add(offset_step);
		let x = (current_offset * angle.cos()) as i64;
		let y = -(current_offset * angle.sin()) as i64;
		let location = (x, y).into();
		let star_system_ids = placer.cluster(rng, u, location, &node.stars);
		if let Some((_, prev_systems)) = locations_ss.last() {
			u.starlanes.add_links(prev_systems.as_slice(), star_system_ids.as_slice());
		};
		locations_ss.push((location, star_system_ids.clone()));

		for (branch_n, branch) in node.branches.iter().enumerate() {
			let branch_end_angle = match branch_n {
				0 => angle.add(ray_angle_step.mul(0.4)),
				1 => angle.sub(ray_angle_step.mul(0.4)),
				_ => panic!("More than two branches are not supported"),
			};

			lay_out_interconnect((current_offset, star_system_ids.clone()), None, angle, branch_end_angle, offset_step, u, placer, branch, rng);
		}
	}
	locations_ss
}

// place locations on a simple line
fn lay_out_line(
	starting_location: UniverseLocation,
	ending_location: UniverseLocation,
	universe: &mut universe::Universe,
	placer: &mut Placer,
	span: &Span,
	rng: &mut impl Rng,
) -> Vec<Vec<ID<universe::StarSystem>>> {
	let mut current_location = starting_location;
	let location_step_x = (ending_location.x.sub(starting_location.x)).div(span.nodes.len() as i64);
	let location_step_y = (ending_location.y.sub(starting_location.y)).div(span.nodes.len() as i64);
	let mut prev_ic_systems_option: Option<Vec<ID<universe::StarSystem>>> = None;
	let mut spine = vec![];
	for node in &span.nodes {
		let ic_star_system_ids = placer.cluster(rng, universe, current_location, &node.stars);

		if let Some(prev_ic_systems) = prev_ic_systems_option {
			universe.starlanes.add_links(prev_ic_systems.as_slice(), ic_star_system_ids.as_slice());
		}

		prev_ic_systems_option = Some(ic_star_system_ids.clone());
		current_location = (current_location.x.add(location_step_x), current_location.y.add(location_step_y)).into();
		spine.push(ic_star_system_ids);
	}
	spine
}

/// place the locations on an interconnect, a piece of a circle around 0.0
#[allow(clippy::too_many_arguments)]
fn lay_out_interconnect(
	starting_cluster: (f32, Vec<ID<universe::StarSystem>>),
	ending_cluster: Option<(f32, Vec<ID<universe::StarSystem>>)>,
	mut angle: f32,
	next_angle: f32,
	// hint about offset step of the previously placed span, to fit branches.
	span_offset_step: f32,
	universe: &mut universe::Universe,
	placer: &mut Placer,
	span: &Span,
	rng: &mut impl Rng,
) {
	let (mut offset, mut prev_ic_systems) = starting_cluster;
	let (target_offset, ending_cluster) = if let Some((target_offset, ending_cluster)) = ending_cluster {
		(target_offset, Some(ending_cluster))
	} else {
		(offset, None)
	};
	let ray_angle_step = next_angle.sub(angle).div(span.nodes.len().add(1) as f32);
	let offset_step = target_offset.sub(offset).div(span.nodes.len().add(1) as f32);
	for node in &span.nodes {
		angle += ray_angle_step;
		offset += offset_step;
		let x = (offset * angle.cos()) as i64;
		let y = -(offset * angle.sin()) as i64;
		let location = (x, y).into();
		let ic_star_system_ids = placer.cluster(rng, universe, location, &node.stars);

		universe.starlanes.add_links(prev_ic_systems.as_slice(), ic_star_system_ids.as_slice());

		prev_ic_systems.clone_from(&ic_star_system_ids);

		for (branch_n, branch) in node.branches.iter().enumerate() {
			let next_branch_offset = match branch_n {
				0 => offset.sub(span_offset_step.mul(0.4)),
				1 => offset.add(span_offset_step.mul(0.4)),
				_ => panic!("More than two branches are not supported"),
			};

			let branch_star_systems = lay_out_span(angle, ray_angle_step, offset, next_branch_offset, branch, universe, placer, rng);
			universe.starlanes.add_links(prev_ic_systems.as_slice(), ic_star_system_ids.as_slice());
			universe.starlanes.add_links(ic_star_system_ids.as_slice(), branch_star_systems[0].1.as_slice());
		}
	}

	if let Some(ending_cluster) = ending_cluster {
		universe.starlanes.add_links(prev_ic_systems.as_slice(), ending_cluster.as_slice());
	};
}

pub fn generate_universe(params: &UniverseParameters) -> Result<universe::Universe> {
	let span = tracing::span!(tracing::Level::DEBUG, "universegen");
	let enter = span.enter();
	let seed = if let Some(seed) = params.seed { seed } else { rand::random() };
	tracing::debug!("seed: {seed}");
	let mut rng = rand_chacha::ChaCha8Rng::seed_from_u64(seed);
	let mut u = universe::Universe::default();
	rng.try_fill(&mut u.id_storage_key)?;

	// first 16 get a distinctive color. More are not recommended.
	fn generate_colored_empires<Rng: rand::Rng>(u: &mut universe::Universe, rng: &mut Rng, empires_n: u64) {
		for (color_counter, _) in (0..empires_n).enumerate() {
			let name = names::generate_name(rng, 5, "an", names::NameGenerator::Autistic);
			let color = match crate::color::DISTINCTIVE_COLORS.get(color_counter) {
				Some(color) => *color,
				None => (rng.gen_range(100..255), rng.gen_range(100..255), rng.gen_range(100..255)).into(),
			};
			u.add_empire(&name, color).unwrap();
		}
	}

	match &params.generator {
		UniverseGenerator::Haphazard => {
			generate_colored_empires(&mut u, &mut rng, params.empires_n);
			let width = params.width.unwrap_or_else(|| params.get_star_systems_total() * 50); // 100 / both directions / both axis
			let height = params.width.unwrap_or(width);

			u.width = width;
			u.height = height;

			let mut placer = Placer::default();

			for _ in 0..params.get_star_systems_total() {
				let location = placer.random_place(&mut rng, &u);
				let mut generated = false;
				for empire_id in u.all_ids() {
					if u.capitals_in_empires.child(empire_id).is_none() {
						let star_system_id = generate_star_system(&mut u, location, StarDna::ColonialCapital, &mut rng);
						u.set_star_system_owner(star_system_id, empire_id);
						u.set_empire_capital(empire_id, star_system_id);
						generated = true;
						break;
					}
				}
				if generated {
					continue;
				};
				generate_star_system(
					&mut u,
					location,
					StarDna::Random(
						*[universe::StarType::White, universe::StarType::Yellow, universe::StarType::Blue, universe::StarType::Red]
							.choose(&mut rng)
							.unwrap(),
					),
					&mut rng,
				);
			}

			generate_starlanes(&mut u);
			u.resize_to_bounding_box(1.2);
		}
		UniverseGenerator::Pentagram => {
			let angle_step = std::f32::consts::PI.mul(2.0).div(5.0);
			let rotate = std::f32::consts::PI.mul(0.5);
			let offset: f32 = 1500.0f32.mul(50.0);
			let mut placer = Placer::default();
			let span_dna = vec![SpanDna {
				clusters: vec![],
				branches: vec![],
				spine_weight: 1.0,
				star_dna_weights: Some(vec![(StarDna::Random(universe::StarType::Red), 0.1)]),
			}];
			let span = Span::build_from_dna(&span_dna, 40.0, &mut rng, true);
			generate_colored_empires(&mut u, &mut rng, 5);
			for empire_n in 0..5 {
				let o_n: usize = empire_n.add(2_usize);
				let other_empire_n: usize = o_n.rem(5);
				let angle = (empire_n as f32).mul(angle_step).add(rotate);
				let x = (offset * angle.cos()) as i64;
				let y = -(offset * angle.sin()) as i64;
				let first_location = (x, y).into();
				let angle = (other_empire_n as f32).mul(angle_step).add(rotate);
				let x = (offset * angle.cos()) as i64;
				let y = -(offset * angle.sin()) as i64;
				let second_location = (x, y).into();
				lay_out_line(first_location, second_location, &mut u, &mut placer, &span.0, &mut rng);
			}
			u.resize_to_bounding_box(1.3);
		}
		UniverseGenerator::Sunrays => {
			/*
			 *         | ic0   | ic1
			 * o-span0-o-span1-o-span2-sol-...
			 *         |       |
			 */

			generate_colored_empires(&mut u, &mut rng, params.empires_n);
			let intercluster_length = 1500.0;

			let interconnect0_weight = 2.0;
			let interconnect1_weight = 1.0;
			let span0_weight = 1.0;
			let span1_weight = 1.0;
			let span2_weight = 1.0;
			let weights = interconnect0_weight + interconnect1_weight + span0_weight + span1_weight + span2_weight;

			let span0_dna = vec![
				SpanDna {
					clusters: vec![ExpansionOption { size: 2, weight: 1.0 }, ExpansionOption { size: 3, weight: 1.2 }],
					branches: vec![ExpansionOption { size: 4, weight: 2.0 }, ExpansionOption { size: 3, weight: 1.2 }],
					spine_weight: 1.0,
					star_dna_weights: Some(vec![
						(StarDna::Random(universe::StarType::White), 1.0),
						(StarDna::Random(universe::StarType::Yellow), 0.1),
						(StarDna::Random(universe::StarType::Blue), 0.1),
						(StarDna::Random(universe::StarType::Red), 0.1),
					]),
				},
				SpanDna {
					clusters: vec![ExpansionOption { size: 2, weight: 1.0 }, ExpansionOption { size: 3, weight: 1.2 }],
					branches: vec![],
					spine_weight: 1.0,
					star_dna_weights: None,
				},
			];
			let span1_dna = &span0_dna;
			let span2_dna = &span0_dna;

			let interconnect0_dna = &span0_dna;
			let interconnect1_dna = &span0_dna;

			let sse = params.star_systems_per_empire as f64 / weights;
			let interconnect0_budget = sse * interconnect0_weight;
			let interconnect1_budget = sse * interconnect1_weight;
			let span0_budget = sse * span0_weight;
			let span1_budget = sse * span1_weight;
			let span2_budget = sse * span2_weight;

			let _sol_star_system_id = u
				.insert(universe::StarSystem {
					location: (0, 0).into(),
					name: "sol".into(),
					star_cosmic_ray_intensity: 1000.into(),
					id: ID::invalid(),
					star_type: universe::StarType::Blue,
				})
				.unwrap();
			let empires: Vec<_> = u.empires.values().map(|e| e.id).enumerate().collect();
			let mut prev_empire_roots: Option<Vec<_>> = None;
			let mut prev_tier0_connection: Option<(f32, Vec<_>)> = None;
			let mut first_angle = 0.0;
			let mut first_tier0_connection: Option<(f32, Vec<_>)> = None;
			let mut prev_tier1_connection: Option<(f32, Vec<_>)> = None;
			let mut first_tier1_connection: Option<(f32, Vec<_>)> = None;
			let mut first_empire_roots = None;

			let mut placer = Placer::default();
			for (empire_num, empire_id) in empires.clone() {
				let (span0, leftover_budget) = Span::build_from_dna(&span0_dna, span0_budget, &mut rng, true);
				tracing::debug!("span0={}, {leftover_budget}", span0.nodes.len());
				let (span1, leftover_budget) = Span::build_from_dna(span1_dna, span1_budget.add(leftover_budget), &mut rng, true);
				tracing::debug!("span1={}, {leftover_budget}", span1.nodes.len());
				let (mut span2, leftover_budget) = Span::build_from_dna(span2_dna, span2_budget.add(leftover_budget), &mut rng, true);
				tracing::debug!("span2={}, {leftover_budget}", span2.nodes.len());

				// force capital system into span2
				span2.nodes.push(Node {
					stars: vec![StarDna::ColonialCapital],
					branches: vec![],
				});

				tracing::debug!("interconnect0");
				let (interconnect0_span, leftover_budget) = Span::build_from_dna(interconnect0_dna, interconnect0_budget.add(leftover_budget), &mut rng, false);
				tracing::debug!("interconnect1");
				let (interconnect1_span, leftover_budget) = Span::build_from_dna(interconnect1_dna, interconnect1_budget.add(leftover_budget), &mut rng, false);
				if leftover_budget.round().abs() >= 1.0 {
					tracing::warn!("leftover budget after empire space generation: {leftover_budget}");
				};

				let empire_angle = |e_n| 2. * std::f32::consts::PI / empires.len() as f32 * e_n as f32;
				let ray_angle = empire_angle(empire_num);
				let next_ray_angle = empire_angle(empire_num + 1);
				let ray_angle_step = next_ray_angle.sub(ray_angle);

				let span0_offset = intercluster_length.mul(span0.nodes.len() as f32 + 1.);
				let span0_systems = lay_out_span(ray_angle, ray_angle_step, intercluster_length, span0_offset, &span0, &mut u, &mut placer, &mut rng);
				let span1_offset = span0_offset + intercluster_length.mul(span1.nodes.len() as f32);
				let span1_systems = lay_out_span(ray_angle, ray_angle_step, span0_offset, span1_offset, &span1, &mut u, &mut placer, &mut rng);
				let span2_offset = span1_offset + intercluster_length.mul(span2.nodes.len() as f32);
				let span2_systems = lay_out_span(ray_angle, ray_angle_step, span1_offset, span2_offset, &span2, &mut u, &mut placer, &mut rng);

				if let (Some((_, last_systems)), Some((_, first_systems))) = (span0_systems.last(), span1_systems.first()) {
					u.starlanes.add_links(last_systems, first_systems);
				};

				if let (Some((_, last_systems)), Some((_, first_systems))) = (span1_systems.last(), span2_systems.first()) {
					u.starlanes.add_links(last_systems, first_systems);
				};

				// Link the first root to the previous one
				if let (Some(prev_empire_roots), Some((_, empire_roots))) = (&prev_empire_roots, span0_systems.first()) {
					u.starlanes.add_links(empire_roots, prev_empire_roots.as_slice());
				}

				if let (0, Some((_, empire_roots))) = (empire_num, span0_systems.first()) {
					first_empire_roots = Some(empire_roots.clone());
				};

				if let (Some(first_empire_roots), Some((_, empire_roots))) = (&first_empire_roots, span0_systems.first()) {
					if empire_num == empires.len() - 1 {
						u.starlanes.add_links(empire_roots, first_empire_roots.as_slice());
					}
				};

				let tier0_connection = (span0_offset, span0_systems.last().unwrap().clone().1);
				let tier1_connection = (span1_offset, span1_systems.last().unwrap().clone().1);

				if empire_num == 0 {
					first_angle = ray_angle;
					first_tier0_connection = Some(tier0_connection.clone());
					first_tier1_connection = Some(tier1_connection.clone());
				}

				if empire_num >= 1 {
					let prev_angle = 2. * std::f32::consts::PI / params.empires_n as f32 * (empire_num - 1) as f32;
					lay_out_interconnect(
						tier0_connection.clone(),
						Some(prev_tier0_connection.clone().unwrap()),
						ray_angle,
						prev_angle,
						intercluster_length,
						&mut u,
						&mut placer,
						&interconnect0_span,
						&mut rng,
					);
					lay_out_interconnect(
						tier1_connection.clone(),
						Some(prev_tier1_connection.clone().unwrap()),
						ray_angle,
						prev_angle,
						intercluster_length,
						&mut u,
						&mut placer,
						&interconnect1_span,
						&mut rng,
					);
				}

				if empire_num == empires.len() - 1 {
					lay_out_interconnect(
						tier0_connection.clone(),
						Some(first_tier0_connection.clone().unwrap()),
						ray_angle,
						first_angle.add(std::f32::consts::PI.mul(2.0)),
						intercluster_length,
						&mut u,
						&mut placer,
						&interconnect0_span,
						&mut rng,
					);
					lay_out_interconnect(
						tier1_connection.clone(),
						Some(first_tier1_connection.clone().unwrap()),
						ray_angle,
						first_angle.add(std::f32::consts::PI.mul(2.0)),
						intercluster_length,
						&mut u,
						&mut placer,
						&interconnect1_span,
						&mut rng,
					);
				}
				prev_tier0_connection = Some(tier0_connection);
				prev_tier1_connection = Some(tier1_connection);

				// place capital in tier2 span
				let capital_cluster = &span2_systems[span2_systems.len() - 1];
				let capital_system = capital_cluster.1[0];
				u.set_star_system_owner(capital_system, empire_id);
				u.set_empire_capital(empire_id, capital_system);

				prev_empire_roots = span0_systems.first().map(|f| f.1.clone());
			}
			u.resize_to_bounding_box(1.2);
		}
		// Universe generated by this should function well enough from turn 1 to be able to test most mechanics. Thus it is seeded with fuel and assets.
		UniverseGenerator::TheGrid => {
			fn singleton(u: &mut universe::Universe, star_type: universe::StarType, name: &str, location: UniverseLocation) -> ID<universe::StarSystem> {
				let star_system = universe::StarSystem {
					id: ID::invalid(),
					name: name.into(),
					location,
					star_cosmic_ray_intensity: 0.into(),
					star_type,
				};
				let star_system_id = u.insert(star_system).unwrap();
				let mut planet = universe::Planet {
					number: u.planets_in_star_systems.children(star_system_id).len() as i64,
					..Default::default()
				};
				planet.set_natural_fuel_availability();
				let planet_id = u.insert(planet).unwrap();
				u.planets_in_star_systems.add_link(star_system_id, planet_id);
				star_system_id
			}

			fn theplanet(u: &mut universe::Universe, star_system_id: ID<universe::StarSystem>) -> &mut universe::Planet {
				u.getf_mut(u.planets_in_star_systems.children(star_system_id)[0]).unwrap()
			}

			fn rock(u: &mut universe::Universe, planet_id: ID<universe::Planet>) {
				basic_settlers(u, planet_id, 1_000_000.into());
				let planet = u.getf_mut(planet_id).unwrap();
				planet.surface_bio_quality = 0.0.into();
				planet.atmosphere_bio_quality = 0.0.into();
				planet.core_spin_speed = 0.0.into();
				planet.set_natural_fuel_availability();
				planet.economic_asset_list.fuel_sifters = 1e6.into();
			}

			fn semi(u: &mut universe::Universe, planet_id: ID<universe::Planet>) {
				basic_settlers(u, planet_id, 1_000_000.into());
				let planet = u.getf_mut(planet_id).unwrap();
				planet.surface_bio_quality = 0.7.into();
				planet.atmosphere_bio_quality = 0.3.into();
				planet.core_spin_speed = 0.5.into();
				planet.set_natural_fuel_availability();
			}

			fn breadbasket(u: &mut universe::Universe, planet_id: ID<universe::Planet>) {
				basic_settlers(u, planet_id, 1_000_000.into());
				let planet = u.getf_mut(planet_id).unwrap();
				planet.surface_bio_quality = 1.0.into();
				planet.atmosphere_bio_quality = 1.0.into();
				planet.core_spin_speed = 1.0.into();
				planet.set_natural_fuel_availability();
				planet.economic_asset_list.farms = 4e6.into();
			}

			fn condo(u: &mut universe::Universe, planet_id: ID<universe::Planet>) {
				basic_settlers(u, planet_id, 2_000_000.into());
				let planet = u.getf_mut(planet_id).unwrap();
				planet.surface_bio_quality = 0.9.into();
				planet.atmosphere_bio_quality = 1.0.into();
				planet.core_spin_speed = 1.0.into();
				planet.set_natural_fuel_availability();
			}

			fn basic_settlers(u: &mut universe::Universe, planet_id: ID<universe::Planet>, settlers: Unit<Population>) {
				let planet = u.getf_mut(planet_id).unwrap();
				planet.population = settlers;
				planet.goods_storage.fuel = settlers.mul(0.5).into();
				planet.goods_storage.energy_credits = settlers.cast();
				planet.goods_storage.food = settlers.mul(2.0).into();
				u.void_church.goods_storage.fuel.add_assign(settlers.cast());
			}

			let e1 = u.add_empire("Empire1", (0, 255, 0).into()).unwrap();
			u.getf_mut(e1).unwrap().tax_rate = 0;
			let e2 = u.add_empire("Empire2", (255, 0, 0).into()).unwrap();

			let intercluster_length = 290.0_f64;
			let width = intercluster_length * 7.;
			let height = intercluster_length * 6.;
			u.width = width as u64;
			u.height = height as u64;
			let topleft = UniverseLocation::from(((intercluster_length.mul(-4.5)).round() as i64, (intercluster_length.mul(-3.5).round() as i64)));

			for system_x in 0..6 {
				for system_y in 0..3 {
					let location = topleft + (intercluster_length.mul(system_x as f64) as i64, intercluster_length.mul(system_y as f64) as i64).into();

					match (system_x, system_y) {
						(0, 0) => {
							let star_system_id = singleton(&mut u, universe::StarType::Red, "rock1-1", location);
							u.set_star_system_owner(star_system_id, e1);
							let planet = theplanet(&mut u, star_system_id).id;
							rock(&mut u, planet);
						}
						(0, 1) => {
							let star_system_id = singleton(&mut u, universe::StarType::Yellow, "capital1", location);
							u.set_star_system_owner(star_system_id, e1);
							u.set_empire_capital(e1, star_system_id);
							let planet = theplanet(&mut u, star_system_id).id;
							semi(&mut u, planet);
						}
						(0, 2) => {
							let star_system_id = singleton(&mut u, universe::StarType::Yellow, "breadbasket1-1", location);
							u.set_star_system_owner(star_system_id, e1);

							let planet = theplanet(&mut u, star_system_id).id;
							breadbasket(&mut u, planet);
						}
						(1, row) => {
							let star_system_id = singleton(&mut u, universe::StarType::White, &format!("rock1-{}", row + 2), location);
							u.set_star_system_owner(star_system_id, e1);
							let planet = theplanet(&mut u, star_system_id).id;
							rock(&mut u, planet);
						}
						(2, 0) => {
							let star_system_id = singleton(&mut u, universe::StarType::Yellow, "breadbasket1-2", location);
							u.set_star_system_owner(star_system_id, e1);
							let planet = theplanet(&mut u, star_system_id).id;
							breadbasket(&mut u, planet);
						}
						(2, 1) => {
							let star_system_id = singleton(&mut u, universe::StarType::Blue, "rock1-5", location);
							u.set_star_system_owner(star_system_id, e1);
							let planet = theplanet(&mut u, star_system_id).id;
							rock(&mut u, planet);
						}
						(2, 2) => {
							let star_system_id = singleton(&mut u, universe::StarType::Yellow, "condo1", location);
							u.set_star_system_owner(star_system_id, e1);
							let planet = theplanet(&mut u, star_system_id).id;
							condo(&mut u, planet);
						}
						(3, 2) => {
							let star_system_id = singleton(&mut u, universe::StarType::Yellow, "breadbasket2-2", location);
							u.set_star_system_owner(star_system_id, e2);
							let planet = theplanet(&mut u, star_system_id).id;
							breadbasket(&mut u, planet);
						}
						(3, 1) => {
							let star_system_id = singleton(&mut u, universe::StarType::Blue, "rock2-5", location);
							u.set_star_system_owner(star_system_id, e2);
							let planet = theplanet(&mut u, star_system_id).id;
							rock(&mut u, planet);
						}
						(3, 0) => {
							let star_system_id = singleton(&mut u, universe::StarType::Yellow, "condo2", location);
							u.set_star_system_owner(star_system_id, e2);
							let planet = theplanet(&mut u, star_system_id).id;
							condo(&mut u, planet);
						}
						(4, row) => {
							let star_system_id = singleton(&mut u, universe::StarType::White, &format!("rock2-{}", (2 - row) + 2), location);
							u.set_star_system_owner(star_system_id, e2);
							let planet = theplanet(&mut u, star_system_id).id;
							rock(&mut u, planet);
						}
						(5, 2) => {
							let star_system_id = singleton(&mut u, universe::StarType::Red, "rock2-1", location);
							u.set_star_system_owner(star_system_id, e2);
							let planet = theplanet(&mut u, star_system_id).id;
							rock(&mut u, planet);
						}
						(5, 1) => {
							let star_system_id = singleton(&mut u, universe::StarType::Yellow, "capital2", location);
							u.set_star_system_owner(star_system_id, e2);
							u.set_empire_capital(e2, star_system_id);
							let planet = theplanet(&mut u, star_system_id).id;
							semi(&mut u, planet);
						}
						(5, 0) => {
							let star_system_id = singleton(&mut u, universe::StarType::Yellow, "breadbasket2-1", location);
							u.set_star_system_owner(star_system_id, e2);
							let planet = theplanet(&mut u, star_system_id).id;
							breadbasket(&mut u, planet);
						}
						_ => {
							panic!("oops, we shouldn't generate this one")
						}
					}
				}
			}
			generate_starlanes(&mut u);
			u.resize_to_bounding_box(1.2);
		}
	}

	if params.add_testables {
		let ui = u.interface();
		let empires = ui.empires();
		let e1 = empires.get(0).ok_or_else(|| anyhow!("Not enough empires for test"))?;
		let e2 = empires.get(1).ok_or_else(|| anyhow!("Not enough empires for test"))?;
		let ss1_id = e1.capital().ok_or_else(|| anyhow!("Capitalless empire"))?.data.id;
		let ss2_id = e2.capital().ok_or_else(|| anyhow!("Capitalless empire"))?.data.id;
		let e1_id = e1.data.id;
		let e2_id = e2.data.id;
		u.getf_mut(e1_id)?.name = "Empire1".to_string();
		u.getf_mut(e2_id)?.name = "Empire2".to_string();
		u.getf_mut(ss1_id)?.name = "StarSystem1".to_string();
		u.getf_mut(ss2_id)?.name = "StarSystem2".to_string();
		u.starlanes.add_link(ss1_id, ss2_id);
	}

	if let Some(pops) = params.fixed_population {
		let pops_i: i64 = pops.try_into()?;
		let pops_u = Unit::from(pops_i);
		for planet in u.planets.values_mut() {
			planet.population = pops_u;
		}
	} else if params.auto_population {
		// populate planets weighted by inverse insurance cost
		let interface = u.interface();
		let planets = interface.planets();
		let planets_weights = planets.iter().map(|planet| (planet.data.id, 1.0 / planet.insurance_cost().to_f64().unwrap()));
		let total_weights: f64 = planets_weights.clone().map(|p| p.1).sum();
		let total_population = planets.len() as f64;
		let planet_pops: Vec<_> = planets_weights
			.map(|(planet_id, weight)| {
				let pop = total_population / total_weights * weight;
				(planet_id, pop)
			})
			.collect();
		for (planet_id, pop) in planet_pops {
			let planet = u.getf_mut(planet_id)?;
			planet.population += pop.into();
		}
	}

	if params.colonization_packages {
		for planet in u.planets.values_mut() {
			planet.goods_storage.fuel = planet.population.div(2).cast();
			planet.goods_storage.food = planet.population.mul(2).cast();
		}
	}

	if params.molotov_ribbentrop {
		let mut desired_systems = u.star_systems.len() - u.star_systems.len() % u.empires.len() - 1;
		let empires = u.empires.iter().cycle();
		for ((_, star_system), (_, empire)) in u.star_systems.iter().zip(empires) {
			if desired_systems > 0 && u.capitals_in_empires.parent(star_system.id).is_none() {
				u.star_systems_in_empires.purge_child(star_system.id);
				u.star_systems_in_empires.add_link(empire.id, star_system.id);
				desired_systems -= 1;
			}
		}
	}

	if params.spawn_lots_of_ships {
		let owners: Vec<_> = u.empires.values().map(|v| v.id).collect();
		let systems: Vec<_> = u.star_systems.values().map(|v| v.id).collect();
		for system in systems {
			for owner in &owners {
				let number = rng.gen_range(0..5);
				if number > 0 {
					let fleet_id = u.add_fleet(&names::generate_name(&mut rng, 7, "or", names::NameGenerator::Autistic), system, *owner)?;
					for _ in 0..number {
						u.add_ship(&names::generate_name(&mut rng, 5, "en", names::NameGenerator::Autistic), fleet_id)?;
					}
				}
			}
		}
	}
	let mut b = crate::util::benchmark::Benchmark::default();
	u.calculate_trading_parents();
	b.milestone("calculated trading parents");
	drop(enter);
	Ok(u)
}

fn generate_starlanes(u: &mut universe::Universe) {
	let max_starlane_length = 300.;
	for system in u.star_systems.values() {
		for potential_system in u.star_systems.values() {
			if system != potential_system && distance(potential_system.location, system.location) < max_starlane_length {
				u.starlanes.add_link(system.id, potential_system.id);
			}
		}
	}
}

fn distance(l1: universe::UniverseLocation, l2: universe::UniverseLocation) -> f32 {
	(((l1.x - l2.x).pow(2) + (l1.y - l2.y).pow(2)) as f32).sqrt()
}
