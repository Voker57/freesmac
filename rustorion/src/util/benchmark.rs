use std::time::Instant;

pub struct Benchmark {
	last_milestone: Instant,
}

impl Default for Benchmark {
	fn default() -> Self {
		Benchmark { last_milestone: Instant::now() }
	}
}

impl Benchmark {
	pub fn milestone(&mut self, message: &str) {
		tracing::debug!("Milestone {}: {:?}", message, self.last_milestone.elapsed());
		self.last_milestone = Instant::now();
	}
}
